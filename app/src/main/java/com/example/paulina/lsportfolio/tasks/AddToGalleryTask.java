package com.example.paulina.lsportfolio.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;


public class AddToGalleryTask extends AsyncTask<Void, Void, Bitmap> {
    private ImageView iv;
    private String path;
    private Bitmap bmp;

    public AddToGalleryTask(ImageView iv, String path){
        this.path = path;
        this.iv = iv;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            InputStream in = new URL(path).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            iv.setImageBitmap(bmp);
        }
    }
}
