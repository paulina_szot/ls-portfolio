package com.example.paulina.lsportfolio.models;

import com.google.gson.annotations.SerializedName;

public class DataResponse2 {
    @SerializedName("data")
    private App data;

    public App getData() {
        return data;
    }

    public void setData(App data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "data=" + data +
                '}';
    }
}
