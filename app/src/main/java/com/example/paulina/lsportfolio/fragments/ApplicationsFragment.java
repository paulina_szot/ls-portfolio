package com.example.paulina.lsportfolio.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.paulina.lsportfolio.models.DataResponse;
import com.example.paulina.lsportfolio.adapter.MyArrayAdapter;
import com.example.paulina.lsportfolio.models.Portfolio;
import com.example.paulina.lsportfolio.R;
import com.example.paulina.lsportfolio.api.APIController;
import com.example.paulina.lsportfolio.api.APIService;
import com.example.paulina.lsportfolio.models.Applications;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Retrofit;

@EFragment(R.layout.fragment_applications)
public class ApplicationsFragment extends Fragment implements Callback<DataResponse> {

    @ViewById(R.id.list)
    protected ListView listView;

    @ViewById(R.id.swipeContainer)
    protected SwipeRefreshLayout swipeContainer;


    private static Context context;

    private  List<Portfolio> list = new ArrayList<Portfolio>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_applications,
                container, false);

        return view;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @AfterViews
    protected void afterViews(){
        if(list.size() == 0)
        getData();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               getData();
            }
        });

    }


    private void getData(){
        APIService apiService = APIController.getAPIService();

        Call<DataResponse> call = apiService.getAll();
        call.enqueue(this);
    }

    @Override
    public void onResponse(retrofit.Response<DataResponse> response, Retrofit retrofit) {
        swipeContainer.setRefreshing(false);
        Applications apps = (Applications) response.body().getData();
       list = apps.getPortfolio();
        getAdapter();
    }

    @Override
    public void onFailure(Throwable t) {
    }

    @Override
    public void onResume()
    {
        super.onResume();
getAdapter();
    }

    private void getAdapter(){
        if(list.size() > 0) {
            MyArrayAdapter adapter = new MyArrayAdapter(context, list, listView);
            listView.setAdapter(adapter);
        }
    }

}
