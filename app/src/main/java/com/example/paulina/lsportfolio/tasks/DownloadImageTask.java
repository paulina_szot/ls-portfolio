package com.example.paulina.lsportfolio.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;


public class DownloadImageTask extends AsyncTask<Void, Void, Void> {
    private ImageView iv;
    private String path;
    private TextView tv;
    private String text;
    private Bitmap bmp;

    public DownloadImageTask(ImageView iv, String path, TextView tv, String text){
        this.tv = tv;
        this.path = path;
        this.iv = iv;
        this.text = text;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            InputStream in = new URL(path).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (bmp != null) {
            iv.setImageBitmap(bmp);
            tv.setText(text);
        }
    }
}
