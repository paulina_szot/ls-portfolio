package com.example.paulina.lsportfolio.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Applications {

    @SerializedName("portfolio")
    private List<Portfolio> portfolio;

    public List<Portfolio> getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(List<Portfolio> portfolio) {
        this.portfolio = portfolio;
    }

    @Override
    public String toString() {
        return "Applications{" +
                "portfolio=" + portfolio +
                '}';
    }
}
