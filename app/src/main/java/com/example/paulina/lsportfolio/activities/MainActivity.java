package com.example.paulina.lsportfolio.activities;


import android.app.Fragment;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.paulina.lsportfolio.R;
import com.example.paulina.lsportfolio.fragments.ApplicationsFragment_;
import com.example.paulina.lsportfolio.fragments.ContactFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;



@EActivity
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.drawer)
    protected DrawerLayout drawer;

    @ViewById(R.id.menu)
    protected ListView menu;

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.back)
    protected ImageView back;

    @ViewById(R.id.toolbar_title)
    protected TextView toolbarTitle;

    private Context context;

    private static Fragment currentFragment;

    private ApplicationsFragment_ fragment;
    private ContactFragment_ fragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @AfterViews
    protected void afterViews(){
        context = MainActivity.this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        String[] menuItems = getResources().getStringArray(R.array.menu_items);
        menu.setAdapter(new ArrayAdapter<String>(this,
                R.layout.menu_item, menuItems));
        menu.setOnItemClickListener(new DrawerItemClickListener());

        fragment = new ApplicationsFragment_();
        fragment.setContext(context);

        if(currentFragment == null)
            currentFragment = fragment;

        if(currentFragment instanceof ApplicationsFragment_){
            back.setImageResource(R.drawable.menu_icon);
            toolbarTitle.setText(context.getString(R.string.apps));
        }
        else{
            back.setImageResource(R.drawable.back_icon);
            toolbarTitle.setText(context.getString(R.string.contact));
        }

        getFragmentManager().beginTransaction().replace(R.id.container, currentFragment).commit();

        fragment2 = new ContactFragment_();

    }


    @Click(R.id.back)
    protected void back(){
       if(currentFragment instanceof ApplicationsFragment_){
           drawer.openDrawer(Gravity.LEFT);
       }
        else if(currentFragment instanceof ContactFragment_){
           currentFragment = fragment;
           back.setImageResource(R.drawable.menu_icon);
           toolbarTitle.setText(context.getString(R.string.apps));
           getFragmentManager().popBackStackImmediate();
       }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            switch(position){
                case 0:
                    currentFragment = fragment;
                    back.setImageResource(R.drawable.menu_icon);
                    toolbarTitle.setText(context.getString(R.string.apps));
                    getFragmentManager().beginTransaction().replace(R.id.container, currentFragment).commit();
                    break;
                case 1:
                    currentFragment = fragment2;
                    back.setImageResource(R.drawable.back_icon);
                    toolbarTitle.setText(context.getString(R.string.contact));
                    getFragmentManager().beginTransaction().replace(R.id.container, currentFragment).addToBackStack(context.getString(R.string.apps)).commit();
                    break;
            }

            drawer.closeDrawers();
        }
    }


}
