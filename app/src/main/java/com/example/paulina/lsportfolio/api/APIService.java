package com.example.paulina.lsportfolio.api;

import com.example.paulina.lsportfolio.models.DataResponse;
import com.example.paulina.lsportfolio.models.DataResponse2;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;


public interface APIService {

    @GET("main")
    Call<DataResponse> getAll();

    @GET("product/{id}")
    Call<DataResponse2> getOne(@Path("id") int id);
}
