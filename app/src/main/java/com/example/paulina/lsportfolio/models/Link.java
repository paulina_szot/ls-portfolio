package com.example.paulina.lsportfolio.models;

import com.google.gson.annotations.SerializedName;

public class Link {

    @SerializedName("url")
    private String url;

    @SerializedName("image")
    private String image;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
