package com.example.paulina.lsportfolio.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.paulina.lsportfolio.tasks.AddToGalleryTask;

import java.util.List;

public class ImageAdapter extends PagerAdapter {
    Context context;
    private List<String> gallery;

    public ImageAdapter(Context context, List<String> gallery){
        this.context=context;
        this.gallery = gallery;

    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    @Override
    public int getCount() {
        return gallery.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            AddToGalleryTask task = new AddToGalleryTask(imageView, gallery.get(position));
            task.execute();
            ((ViewPager) container).addView(imageView, 0 );
            return imageView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
