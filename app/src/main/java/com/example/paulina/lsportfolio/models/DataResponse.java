package com.example.paulina.lsportfolio.models;

import com.google.gson.annotations.SerializedName;

public class DataResponse {
    @SerializedName("data")
    private Applications data;

    public Applications getData() {
        return data;
    }

    public void setData(Applications data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "data=" + data +
                '}';
    }
}
