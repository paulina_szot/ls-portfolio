package com.example.paulina.lsportfolio.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class App {

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("gallery")
    private List<String> gallery;

    @SerializedName("link")
    private List<Link> link;

    @SerializedName("icon")
    private String icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(List<Link> link) {
        this.link = link;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "App{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", gallery=" + gallery +
                ", link=" + link +
                '}';
    }
}
