package com.example.paulina.lsportfolio.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.paulina.lsportfolio.tasks.DownloadImageTask;
import com.example.paulina.lsportfolio.adapter.ImageAdapter;
import com.example.paulina.lsportfolio.models.Link;
import com.example.paulina.lsportfolio.R;
import com.example.paulina.lsportfolio.api.APIController;
import com.example.paulina.lsportfolio.api.APIService;
import com.example.paulina.lsportfolio.models.App;
import com.example.paulina.lsportfolio.models.DataResponse2;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

@EActivity
public class DetailsActivity extends AppCompatActivity implements Callback<DataResponse2> {

    @ViewById(R.id.drawer)
    protected DrawerLayout drawer;

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.icon)
    protected ImageView icon;

    @ViewById(R.id.name)
    protected TextView name;

    @ViewById(R.id.description)
    protected TextView description;

    @ViewById(R.id.back)
    protected ImageView back;

    @ViewById(R.id.toolbar_title)
    protected TextView toolbarTitle;

    @ViewById(R.id.view_pager)
    protected ViewPager viewPager;

    @ViewById(R.id.appstore)
    protected ImageView appstore;

    @ViewById(R.id.gplay)
    protected ImageView gplay;

    @ViewById(R.id.wstore)
    protected ImageView wstore;

    private Context context;

    private App app;

    private String android;

    private String windows;

    private String apple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }

    @AfterViews
    protected void afterViews() {
        context = DetailsActivity.this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent details = getIntent();
        int id = details.getIntExtra("id", 0);
        APIService apiService = APIController.getAPIService();
        Call<DataResponse2> call = apiService.getOne(id);
        call.enqueue(this);
    }

    @Click(R.id.back)
    protected void back(){
        super.onBackPressed();
}

    @Click(R.id.appstore)
    protected void appstoreClick(){
        if(app != null){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(apple));
            startActivity(browserIntent);
        }
    }

    @Click(R.id.gplay)
    protected void gplayClick(){
        if(app != null){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(android));
            startActivity(browserIntent);
        }
    }

    @Click(R.id.wstore)
    protected void wstoreClick(){
        if(app != null){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(windows));
            startActivity(browserIntent);
        }
    }

    @Override
    public void onResponse(Response<DataResponse2> response, Retrofit retrofit) {
        app = (App) response.body().getData();
        DownloadImageTask task = new DownloadImageTask(icon, app.getIcon(), name, app.getName());
        task.execute();
        toolbarTitle.setText(app.getName());
        name.setText(app.getName());
        description.setText(app.getDescription());
        ImageAdapter adapter = new ImageAdapter(context, app.getGallery());
        viewPager.setAdapter(adapter);
        for(Link s: app.getLink()){
            if(s.getImage().equals(context.getString(R.string.type1))){
                android = s.getUrl();
                gplay.setVisibility(View.VISIBLE);
            }
            else if(s.getImage().equals(context.getString(R.string.type2))){
                apple = s.getUrl();
                appstore.setVisibility(View.VISIBLE);
            }
            else if(s.getImage().equals(context.getString(R.string.type3))){
                windows = s.getUrl();
                wstore.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFailure(Throwable t) {}
}
