package com.example.paulina.lsportfolio.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.paulina.lsportfolio.activities.DetailsActivity_;
import com.example.paulina.lsportfolio.models.Portfolio;
import com.example.paulina.lsportfolio.R;
import com.example.paulina.lsportfolio.tasks.DownloadImageTask;

import java.util.List;

public class MyArrayAdapter extends ArrayAdapter<Portfolio> {

    private Context context;
    private List<Portfolio> list;
    private ListView listView;
    private ImageView icon;
    private TextView name;

    public MyArrayAdapter(Context context, List<Portfolio> list, ListView listview) {
        super(context,android.R.layout.simple_list_item_1, list);
        this.context = context;
        this.list = list;
        this.listView = listview;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.row, parent, false);
        if (position % 2 == 0) {
           convertView.setBackgroundColor(context.getResources().getColor(R.color.grey));
        } else {
           convertView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        name = (TextView) convertView.findViewById(R.id.appname);
        icon = (ImageView) convertView.findViewById(R.id.appicon);
        icon.setTag(list.get(position).getIcon());
        DownloadImageTask task = new DownloadImageTask(icon, list.get(position).getIcon(), name, list.get(position).getName());
        task.execute();
        addLongActionListener();
        return convertView;
    }

    private void addLongActionListener(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               view.setBackgroundColor(context.getResources().getColor(R.color.orange));
              Intent details = new Intent(context, DetailsActivity_.class);
                details.putExtra("id", list.get(position).getId());
                context.startActivity(details);
            }
        });
    }

}
